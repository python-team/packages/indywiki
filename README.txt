INDYWIKI 0.9.9
-----------------------------------
This is the source distribution for indywiki (previously known as Indywikia,
renamed  after a kind request from Wikia Inc).

Software dependencies
-----------------------------------

* python 2.4 or later (earlier versions may work but haven't been tested)
* PyQT 4.3

Linux + Unix + Mac OS
-----------------------------------

Download and install PyQt4, either from sources, or from a software management
system (on debian based systems, that is apt-get install python-qt4).

Change to the directory were you have extracted indywiki and run:
$cd src/indywiki 
$python indywiki.py

or to install the program, run as root
#python setup.py install


Windows
-----------------------------------

We have included windows executables, for windows users to be able to run the
program without having to get python, pyqt and qt. However, by making this an
executable (using py2exe program), it contains python+pyqt4 on modules as
windows DLL's, that's why the size is extremely big (~23mb).  Download the
windows zip, unzip it somewhere and click on the indywiki icon for the program
to launch.  As an alternative, you can download and install python, qt, sip
and pyqt4 modules and run the code, instead of the executable.  Also, keep in
mind that indywiki is not tested extensively on windows , since the
development takes place on linux.

Hope you will find the program useful :)

http://indywiki.sourceforge.net


