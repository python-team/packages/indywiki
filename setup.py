from setuptools import setup, find_packages
import os

execfile(os.path.join('src', 'indywiki', 'release.py'))

setup(name = 'indywiki',
        version = __version__,
        description = 'visual browser for wikipedia',
        author = __authors__,
        url = 'http://indywiki.sf.net',
        license = __license__,
        packages = find_packages('src'),
        package_dir = {'' : 'src'},
        entry_points = { 'console_scripts' : [ 'indywiki = indywiki.indywiki:main' ] }
)
