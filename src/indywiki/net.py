import threading
import urllib, urllib2
from xml.dom import minidom
from sgmllib import SGMLParser
import httplib
import StringIO
import gzip
from PyQt4 import QtCore, QtGui
from web1 import wikipedia

wikipedia.search = lambda q, language, light:\
        wikipedia.WikipediaSearch(q, language, light, wait=20, asynchronous=False,
                cached=False, case_sensitive=True, full_strip=False)

class queryAPI(threading.Thread):
    """enables threading, and thus accelarates the procedure of finding
valid photo's and their info.otherwise, each proccess would have to
wait the previous one to complete, before it can start the query.If
a wikipedia article hasn't any images, we re-try till we find one,
looking on another link. If we consume all number of links for a site
-we remove a link after we use it's first image-, which happens quite
rarely but is still a case, we feed it with the initial list."""
    def __init__(self, something, n, ui):
        self.url = something.replace('/wiki/', '')
        self.n = n
        self.ui = ui
        threading.Thread.__init__(self)

    def run(self):
        if len(self.ui.sorted_list)<=2:
            self.aquery1 = wikipedia.search("%s" % str(self.ui.sorted_list[0] \
                .encode("utf8").replace('/wiki/', '')), \
                language=self.ui.config.language, light=False)
            self.links2 = self.aquery1.links
            self.ui.sorted_list.extend(self.links2)
            self.aquery2 = wikipedia.search("%s" % str(self.ui.sorted_list[1] \
               .encode("utf8").replace('/wiki/', '')), language=self.ui.config.language, \
               light=False)
            self.links3 = self.aquery2.links
            self.ui.sorted_list.extend(self.links3)
        try:
            self.printme = getImages(self.url.replace('%20', '_') \
                                        .replace(' ', '_').encode("utf8"),
                                        self.ui)
        except IOError:
            self.ui.centralwidget.setCursor(QtGui.QCursor(QtCore.Qt.ArrowCursor))
            return
        try:
            if self.printme['0'][1] in self.ui.displayed_so_far:
                self.ui.dicta['%s' % self.n] = (self.url, self.printme['1'])
            else:
                self.ui.dicta['%s' % self.n] = (self.url, self.printme['0'])
            self.temp0 = self.ui.dicta['%s' % self.n]
            self.ui.imageurl[self.n] = urllib2.Request(self.temp0[1][1], \
                None, self.ui.config.http_headers)
            self.ui.response[self.n] = urllib2.urlopen(self.ui.imageurl[self.n])
            self.ui.imagedata[self.n] = self.ui.response[self.n].read()
            self.ui.response[self.n].close()
            self.ui.ttemp[self.n] = downloadThread(self.n, self.ui)
            self.ui.ttemp[self.n].start()
            self.ui.ttemp[self.n].wait()
        except (KeyError, IndexError, AttributeError):
            if len(self.ui.sorted_list)<=2:
                self.aquery1 = wikipedia.search("%s" % str(
                self.ui.sorted_list[0].encode("utf8").replace('/wiki/', '')), \
                language=self.ui.config.language, light=False)
                self.links2 = self.aquery1.links
                self.ui.sorted_list.extend(self.links2)
                self.aquery2 = wikipedia.search("%s" % str(
                self.ui.sorted_list[1].encode("utf8").replace('/wiki/', '')), \
                language=self.ui.config.language, light=False)
                self.links3 = self.aquery2.links
                self.ui.sorted_list.extend(self.links3)
            self.temp = self.ui.sorted_list[0].replace('/wiki/', '')
            try:
                self.ui.sorted_list.remove(self.temp)
            except ValueError:
                try:
                    self.ui.sorted_list.remove('/wiki/'+self.temp)
                except ValueError:
                    pass
            queryAPI(self.temp, self.n, self.ui).start()


def get_first_ten(ui):
    """prints the first ten images"""
    remaining_images = 10 - len(ui.dicta)
    n = len(ui.dicta)
    while remaining_images != 0:
        if len(ui.sorted_list) >=2:
            kati = ui.sorted_list[0].replace('/wiki/', '')
        else:
            ui.aquery0 = wikipedia.search("%s" % str(ui.sorted_list[0] \
            .encode("utf8").replace('/wiki/', '')), language=ui.config.language, \
            light=False)
            ui.links2 = ui.aquery0.links
            ui.sorted_list.extend(ui.links2)
            kati = ui.sorted_list[0].replace('/wiki/', '')
        try:
            ui.sorted_list.remove(kati)
        except ValueError:
            try:
                ui.sorted_list.remove('/wiki/'+kati)
            except ValueError:
                pass
        queryAPI(kati, n, ui).start()
        remaining_images -= 1
        n += 1



def get_next_ten(ui, number):
    """prints the next ten images! this function replaces the
 ugly get_* functions that were used before"""
    n = len(ui.dicta)
    remaining_images = (number+2)*10 - n
    if n >= (number+2)*10:
        for i in range((number+1)*10, (number+2)*10):
            ui.Download_native(i).start()
    else:
        for i in range((number+1)*10, n):
            ui.Download_native(i).start()
        while remaining_images != 0:
            if len(ui.sorted_list) >= 2:
                kati = ui.sorted_list[0].replace('/wiki/', '')
            else:
                ui.aquery0 = wikipedia.search("%s" % str(
                ui.sorted_list[0].encode("utf8").replace('/wiki/', '')), \
                language=ui.config.language, light=False)
                ui.links2 = ui.aquery0.links
                ui.sorted_list.extend(ui.links2)
                kati = ui.sorted_list[0].replace('/wiki/', '')
            try:
                ui.sorted_list.remove(kati)
            except ValueError:
                try:
                    ui.sorted_list.remove('/wiki/'+kati)
                except ValueError:
                    pass
            queryAPI(kati, n, ui).start()
            remaining_images -= 1
            n += 1
    ui.number_of_ten += 1

def getCategories(ui):
    urllib.URLopener.version = ui.config.agent_id
    categories_usock = urllib.urlopen("http://"+ui.config.language+".wikipedia" + \
    ".org/w/api.php?format=xml&action=query&prop=categories&titles=" + \
    ui.searchKeywords[0][1].encode("utf8").replace(' ', '_'))
    categories_xmldoc = minidom.parse(categories_usock)
    categories_usock.close()
    categories_dom = minidom.parseString(categories_xmldoc.toxml() \
    .encode("utf8"))
    ui.categories_temp_list = categories_dom.getElementsByTagName('cl')
    for i in range(len(ui.categories_temp_list)):
        ui.aquery.categories.append(ui.categories_temp_list[i]  \
            .attributes['title'].value.split(':')[-1])


class downloadThread(QtCore.QThread):
    """python threading calls QThread, because the later worked on a
linear way for me. (threads are not called asynchronously)"""
    def __init__(self, m, ui):
        QtCore.QThread.__init__(self)
        self.n = m
        self.ui = ui

#Now queryAPId emits signals to main, that does the gui processing. All gui
#updates have to be done from the main thread, #and not from other threads...
#I've noticed that QThread emits signals faster than python threading,
#that's why we use python threads to find the info and QThreads to emit the
#signal

    def run(self):
        self.ui.centralwidget.emit(QtCore.SIGNAL("show_nonnative(int)"), self.n)


def getImages(url, ui):
    """returns a dict with images info"""
    request = urllib2.Request("http://"+ui.config.language+".wikipedia.org/wiki/"+url, None, ui.config.http_headers)
    request.add_header('Accept-encoding', 'gzip')        
    opener = urllib2.build_opener()
    try:
        f = opener.open(request)
    except urllib2.URLError: 
        print 'urllib2.Exception' 
        return
    compresseddata = f.read()  
    compressedstream = StringIO.StringIO(compresseddata)   
    gzipper = gzip.GzipFile(fileobj=compressedstream)  
    parser = URLLister()
    parser.feed(gzipper.read())
    parser.close()
    return parser.dict


class URLLister(SGMLParser):
    """class for html parsing"""

    #we set a limit to what will be the minimum analysis of images we download
    def reset(self):
        SGMLParser.reset(self)
        self.number_of_images = 0
        self.dict = {}
        self.links = []

    def start_img(self, attrs):
        alt = [v for k, v in attrs if k == 'alt']
        src = [n for m, n in attrs if m == 'src']
        if URLLister.config.excludeImage(src[0]):
            return
        width = [o for p, o in attrs if p == 'width']
        if len(width) == 0 or int(width[0]) < URLLister.config.min_image_width:
            return
        height = [q for r, q in attrs if r == 'height']
        if len(height) == 0 or int(height[0]) < \
            URLLister.config.min_image_height:
            return
        #not sure why is raised, or if that's the best way to handle it...
        try:
            self.dict["%s" % self.number_of_images] = [alt[0], \
                src[0], width[0], height[0]]
        except IndexError:
            return
        self.number_of_images += 1

    def start_a(self, attrs):
        href = [v for k, v in attrs if k == 'href']
        if href and href[0][:6] == '/wiki/' and not \
            URLLister.config.excludeURL(href):
            self.links.extend(href)

def downloadAndParse(url, agent_id, tagname = "page"):
    urllib.URLopener.version = agent_id
    try:
        usock = urllib.urlopen(url)
    except IOError:
        print 'indywiki: IOError while trying to retrieve %s' % url
        return
    xmldoc = minidom.parse(usock)
    usock.close()
    dom = minidom.parseString(xmldoc.toxml().encode("utf8"))
    return dom.getElementsByTagName(tagname)

