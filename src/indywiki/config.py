import sys
import os
from release import __version__
from web1.wikipedia import languages as known_languages

class Config(object):
    """container for user-configurable options"""

    min_image_width = 90
    min_image_height = 80

    excluded_images = ('/skins-1.5/common/images/poweredby_mediawiki_88x31.png',
            '/images/wikimedia-button.png',
            '/skins-1.5/common/images/magnify-clip.png')
    excluded_urls = ('/wiki/Non-profit_organization', '/wiki/Main_Page',
            '/wiki/Charitable_organization', '/wiki/Talk', '/wiki/Help',
            '/wiki/Image', '/wiki/Templa', '/wiki/Specia', '/wiki/Catego',
            '/wiki/Wikipe', '/wiki/Portal', '/wiki/WP:TON', '/wiki/501%28')

    platform = sys.platform
    agent_id = "indywiki/%s (%s)" % (__version__, platform)
    http_headers = { "User-Agent" : agent_id }

    if platform.startswith("win"):
        config_file = "C:\Documents and Settings\%s\indywiki.ini" % os.environ['USERNAME']
    elif platform.startswith("linux"):
        config_file = os.path.join(os.environ['HOME'], ".indywikirc")
    else:
        config_file = "indywiki.ini"

    language = "en"
    geometry = None
    homePage = None
    nn_img_rgb = (170, 170, 255, 255) # default rgb for non-native images
    show_progress_bar = True
    load_latest_page = True
    confparams = {
         "language" : lambda x: known_languages[x] and x,
         "geometry" : lambda x: x and [int(i) for i in x[1:-1].split(",")],
         "homePage" : lambda x: x,
         "nn_img_rgb" : lambda x: [int(i) for i in x[1:-1].split(",")],
         "show_progress_bar" : lambda x: x != "False",
         "load_latest_page" : lambda x: x != "False"
     }

    def load(self):
        try:
            for line in open(self.config_file):
                if not line.isspace() and not line.startswith("#"):
                    try:
                        parameter, raw_value =\
                            line.replace(" ","").strip().split("=")
                        func = self.confparams[parameter]
                        Config.__setattr__(self, parameter, func(raw_value))
                    except (ValueError, KeyError):
                        print "indywiki: unable to parse '%s' from %s" %\
                                (line, self.config_file)
        except IOError:
            pass
        return (self.geometry, self.homePage)

    def save(self):
        outfile = None
        try:
            try:
                outfile = open(self.config_file, 'w')
                for parameter in [str(p) for p in self.confparams.iterkeys()]:
                    value = Config.__getattribute__(self, parameter)
                    if value != None:
                        outfile.write("%s = %s\n" % (parameter, value))
            except IOError:
                print "indywiki: IOError while writing to %s" % self.config_file
        finally:
            outfile and outfile.close()

    def excludeImage(self, img):
        """returns true if img matches common images we'd rather ignore"""
        return img in self.excluded_images

    def excludeURL(self, url):
        """returns true if url matches common urls we'd rather ignore"""
        return url in self.excluded_urls
